class Board
  attr_accessor :board

  def initialize()
    @board = []
    3.times { |n| @board[n] = [ "   ", "   ", "   " ] }
  end

  def render()
    system("clear")
    @board.each do |i|
      print "-------------------\n|"
      i.each do |j|
          print " #{ j } |"
      end
      print "\n"
    end
    print "-------------------\n"
  end

  def full?
    !( @board.flatten.any? { |value| value == "   "} )
  end

  def free?( location )
    return true if @board[ location[0] ][ location[1] ] == "   "
    return false
  end

  def update( token, location )
    @board[ location[0] ][ location[1] ] = " #{ token.to_s } "
  end

  def check_vertical?( current_player )
    token = " #{ current_player.token } "
    return true if @board[0][0] == token && @board[1][0] == token && @board[2][0] == token
    return true if @board[0][1] == token && @board[1][1] == token && @board[2][1] == token
    return true if @board[0][2] == token && @board[1][2] == token && @board[2][2] == token
    return false
  end

  def check_horizontal?( current_player )
    token = " #{ current_player.token } "
    return true if @board[0][0] == token && @board[0][1] == token && @board[0][2] == token
    return true if @board[1][0] == token && @board[1][1] == token && @board[1][2] == token
    return true if @board[2][0] == token && @board[2][1] == token && @board[2][2] == token
    return false
  end

  def check_diagonal?( current_player )
    token = " #{ current_player.token } "
    return true if @board[0][0] == token && @board[1][1] == token && @board[2][2] == token
    return true if @board[0][2] == token && @board[1][1] == token && @board[2][0] == token
    return false
  end

end


