require_relative './player.rb'
require_relative './board.rb'
system("clear")

class Game

  attr_reader :code
  attr_accessor :board, :game_over

  def initialize()
    @code = rand( 1000...9000 )
    @board = Board.new()
    @game_over = false
    self.create_players
  end

  def create_players
    print "Name for X player: "
    name = gets.chomp.to_s
    @player_x = Player.new( name, "X", @code )

    print "Name for O player: "
    name = gets.chomp.to_s
    @player_o = Player.new( name, "O", @code )
  end

  def update_player
    if @current_player == @player_x
      @current_player = @player_o
    elsif @current_player == @player_o
      @current_player = @player_x
    end
  end

  def any_winner?
    if @board.check_vertical?( @current_player ) || @board.check_horizontal?( @current_player ) ||  @board.check_diagonal?( @current_player )
      @game_over = true
      @current_player.winner = true
    elsif @board.full?
      @game_over = true
    end
  end

  def display_result
    system("clear")
    puts "GAME OVER!\n\nGame code: #{ @code }\nPlayer 1: #{ @player_x.name }, personal code: #{ @player_x.code }.\nPlayer 2: #{ @player_o.name }, personal code: #{ @player_o.code }.\n\n"
    puts "#{ @player_x.name } is the winner" if @player_x.winner?
    puts "#{ @player_o.name } is the winner" if @player_o.winner?
    puts "It's a tie!" if @board.full?
  end

  def player_input
    location = [-1,-1]
    loop do
      location = @current_player.move
      break if @board.free?( location )
    end
    location
  end

  def play
    @current_player = @player_x
    until @game_over
      @board.render
      location = self.player_input
      @board.update( @current_player.token, location )
      @board.render
      self.any_winner?
      self.update_player
    end
    self.display_result
  end
end

game = Game.new()
game.play







