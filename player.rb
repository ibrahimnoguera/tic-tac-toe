class Player
  attr_reader :name, :game_code, :token, :code
  attr_accessor :turn, :winner

  def initialize( name, token, game_code )
    @name = name
    @token = token
    @game_code = game_code.to_s
    @code = rand( 1000..9000 )
    @turn = true
    @winner =  false
  end

  def turn?
     @turn
  end

  def winner?
    @winner
  end

  def move
    print "\nIt is your turn, #{ self.name }. Your token is  #{ self.token }\n\n"
    location_i = -1
    location_j = -1
    until [0,1,2].include?( location_i )
      print "Horizontal value, please: "
      location_i = gets.chomp.to_i
    end
    until [0,1,2].include?( location_j )
      print "Vertical value, please: "
      location_j = gets.chomp.to_i
    end
    return location = [location_i, location_j]
  end

end
